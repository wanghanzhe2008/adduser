#!/bin/bash

# 设置要创建的用户数量
num_users=20

# 设置密码长度
password_length=12

# 设置输出文件
output_file="user_passwords.txt"

echo "开始创建用户..."

# 创建用户并输出用户名和密码
for ((i=1; i<=$num_users; i++))
do
    username="user$i"
    password=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c$password_length)

    # 创建用户
    sudo useradd -m -s /bin/bash $username

    # 设置密码
    echo "$username:$password" | sudo chpasswd

    # 输出用户名和密码到屏幕
    echo "用户 $username 已创建，密码为: $password"

    # 输出用户名和密码到文件
    echo "用户名: $username, 密码: $password" >> $output_file
done

echo "用户创建完成。详细信息已保存到 $output_file 文件中。"
